#ifndef _DATVALUE_H_
#define _DATVALUE_H_

#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue 
{
public:
	virtual TDatValue * GetCopy() = 0; // �������� �����
	~TDatValue() {}
};
#endif
