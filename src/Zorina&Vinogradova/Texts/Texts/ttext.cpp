#include "ttext.h"

int TText::TextLevel;

PTTextLink TText::GetFirstAtom(PTTextLink pl) // ����� ������� �����
{
    PTTextLink tmp = pl;
    while (!tmp->IsAtom())
    {
        St.push(tmp);
        tmp = tmp->GetDown();
    }
    return tmp;
    /*while (!pl->IsAtom())
    {
        St.push(pl);
        pl = pl->GetDown();
    }
    return PTTextLink();*/
}

void TText::PrintText(PTTextLink ptl) // ������ ������ �� ����� ptl
{
    if (ptl != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            cout << ' ';
        cout << ' ' << ptl->Str << endl;
        TextLevel++;
        PrintText(ptl->GetDown());
        TextLevel--;
        PrintText(ptl->GetNext());
    }
}

PTTextLink TText::ReadText(ifstream &TxtFile) //������ ������ �� �����
{
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof())
    {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else
        {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == nullptr)
    {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}


TText::TText(PTTextLink pl)
{
    pFirst = pCurrent = (pl == nullptr) ? new TTextLink() : pl;
}

PTText TText::getCopy()
{
	return 0;
}

// ���������
int TText::GoFirstLink() // ������� � ������ ������
{
    while (!Path.empty())
        Path.pop();
    pCurrent = pFirst;
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    return RetCode;
}

int TText::GoDownLink()  // ������� � ��������� ������ �� Down
{
    if (pCurrent->GetDown() == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetDown();

    }
    return RetCode;
}

int TText::GoNextLink()  // ������� � ��������� ������ �� Next
{
    if (pCurrent->GetNext() == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetNext();
    }
    return RetCode;
}

int TText::GoPrevLink()  // ������� � ���������� ������� � ������
{
    if (pCurrent == pFirst)
        SetRetCode(DataErr);
    else
    {
        pCurrent = Path.top();
        Path.pop();
    }
    return RetCode;
}

// ������
string TText::GetLine()   // ������ ������� ������
{
    return string(pCurrent->Str);
}

void TText::SetLine(string s) // ������ ������� ������ 
{
    pCurrent->Str = s;
}

// �����������
void TText::InsDownLine(string s)    // ������� ������ � ����������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        string buf = s;
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
    }
}

void TText::InsDownSection(string s) // ������� ������� � ����������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        string buf = s;
        pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
    }
}

void TText::InsNextLine(string s)    // ������� ������ � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        string buf = s;
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
    }
}

void TText::InsNextSection(string s) // ������� ������� � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        string buf = s;
        pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
    }
}

void TText::DelDownLine()        // �������� ������ � ���������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown->IsAtom())
        pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection()     // �������� ������� � ���������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pDown = nullptr;
}

void TText::DelNextLine()        // �������� ������ � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection()     // �������� ������� � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

// ��������
int TText::Reset()              // ���������� �� ������ �������
{
    GoFirstLink();
    if (pCurrent != nullptr)
    {
        St.push(pCurrent);
        if (pCurrent->pNext != nullptr)
            St.push(pCurrent->pNext);
        if (pCurrent->pDown != nullptr)
            St.push(pCurrent->pDown);
    }
    return RetCode;
}

bool TText::IsTextEnded() const  // ����� ��������?
{
    return St.empty();
}

int TText::GoNext()             // ������� � ��������� ������
{
    if (!IsTextEnded())
    {
        pCurrent = St.top();
        St.pop();
        if (pCurrent != pFirst)
        {
            if (pCurrent->pNext != nullptr)
                St.push(pCurrent->pNext);
            if (pCurrent->pDown != nullptr)
                St.push(pCurrent->pDown);
        }

    }
    return IsTextEnded();
}

//������ � �������
void TText::Read(char * pFileName)  // ���� ������ �� �����
{
    ifstream TextFile(pFileName);
    TextLevel = 0;
    if (TextFile)
        pFirst = ReadText(TextFile);
}

void TText::Write(char * pFileName) // ����� ������ � ����
{
    ofstream TextFile(pFileName);
    TextLevel = 0;
    if (pFirst != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            TextFile << ' ';
        TextFile << ' ' << pFirst->Str << endl;
        TextLevel++;
        PrintText(pFirst->GetDown());
        TextLevel--;
        PrintText(pFirst->GetNext());
    }
}

//������
void TText::Print()             // ������ ������
{
    TextLevel = 0;
    PrintText(pFirst);
}
