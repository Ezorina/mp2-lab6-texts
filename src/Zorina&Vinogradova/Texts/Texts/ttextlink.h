#ifndef __TEXTLINK_H__
#define __TEXTLINK_H__

#include <iostream>

#include "tdatvalue.h"

using namespace std;

#define MemSize 25
#define TextLineLength 225

class TTextLink;
typedef TTextLink *PTTextLink;
//typedef char TStr[TextLineLength];

class TTextMem 
{
	PTTextLink pFirst;     // ��������� �� ������ �����
	PTTextLink pLast;      // ��������� �� ��������� �����
	PTTextLink pFree;      // ��������� �� ������ ��������� �����
    TTextMem() : pFirst(nullptr), pLast(nullptr), pFree(nullptr) {}
	friend class TTextLink;
};

typedef TTextMem *PTTextMem;
class TText;

class TTextLink : public TDatValue
{
protected:
	//TStr Str;                       // ���� ��� �������� ������ ������
    string Str;
	PTTextLink pNext, pDown;        // ��������� �� ���. ������� � �� ����������
	static TTextMem MemHeader;      // ������� ���������� �������
public:
	static void InitMemSystem(int size = MemSize); // ������������� ������
	static void PrintFreeLink(void);               // ������ ��������� �������
	void * operator new (size_t size);             // ��������� �����
	void operator delete (void *pM);               // ������������ �����
	static void MemCleaner(TText& txt);            // ������ ������
	//TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL)
	//{
	//	pNext = pn;
	//	pDown = pd;
	//	if (s != NULL)
	//		strcpy_s(Str, s); 
	//	else 
	//		Str[0] = '\0';
	//}
    //TTextLink(string s)
    //{
    //    pNext = nullptr;
    //    pDown = nullptr;
    //    strcpy_s(Str, s.c_str());
    //}
    TTextLink(string s = nullptr, PTTextLink pn = nullptr, PTTextLink pd = nullptr)
    {
        pNext = pn;
        pDown = pd;
        Str = s;
    }
	~TTextLink() {}
	bool IsAtom() { return pDown == nullptr; } // �������� ����������� �����
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
    //virtual void Print(ostream &os) { os << Str; }
    virtual void Print() {}
	friend class TText;
};

#endif
