#include "C:\Users\1014308\Desktop\Lab6\mp2-lab6-texts\src\Zorina&Vinogradova\include\ttext.h"

int TText::TextLevel;

PTTextLink TText::GetFirstAtom(PTTextLink pl) // ����� ������� �����
{
    PTTextLink tmp = pl;
    while (!tmp->IsAtom())
    {
        St.push(tmp);
        tmp = tmp->GetDown();
    }
    return tmp;
}

void TText::PrintText(PTTextLink ptl) // ������ ������ �� ����� ptl
{
    if (ptl != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            cout << " ";
        cout << ptl->Str << endl;
        TextLevel++;
        PrintText(ptl->GetDown());
        TextLevel--;
        PrintText(ptl->GetNext());
    }
}

void TText::PrintTextInFile(PTTextLink ptl, ofstream &TxtFile) // ������ ������ � ���� �� ����� ptl
{
    if (ptl != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            TxtFile << " ";
        TxtFile << ptl->Str << endl;
        TextLevel++;
        PrintTextInFile(ptl->GetDown(), TxtFile);
        TextLevel--;
        PrintTextInFile(ptl->GetNext(), TxtFile);
    }
}

PTTextLink TText::ReadText(ifstream &TxtFile) //������ ������ �� �����
{
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof())
    {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else
        {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == nullptr)
    {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}


TText::TText(PTTextLink pl)
{
    if (pl == nullptr)
        pl = new TTextLink;
    pFirst = pl;
}

PTText TText::getCopy() //?
{
    PTTextLink pl1, pl2, pl = pFirst, copied = nullptr;
    bool flag = true;
    
    if (pFirst != nullptr)
    {
        while (!St.empty())
            St.pop();
        while (flag)
        {
            if (pl != nullptr)
            {
                pl = GetFirstAtom(pl);
                St.push(pl);
                pl = pl->GetDown();
            }
            else if (St.empty())
                flag = false;
            else
            {
                pl1 = St.top(); St.pop();
                if (strstr(pl1->Str, "Copy") == nullptr)
                {
                    pl2 = new TTextLink("Copy", pl1, copied);
                    St.push(pl2);
                    pl = pl1->GetNext();
                    copied = nullptr;
                }
                else
                {
                    pl2 = pl1->GetNext();
                    strcpy_s(pl1->Str, pl2->Str);
                    pl1->pNext = copied;
                    copied = pl1;
                }
            }
        }

    }
    return new TText(copied);
}

// ���������
int TText::GoFirstLink() // ������� � ������ ������
{
    while (!Path.empty())
        Path.pop();
    pCurrent = pFirst;
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    return RetCode;
}

int TText::GoDownLink()  // ������� � ��������� ������ �� Down
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetDown();

    }
    return RetCode;
}

int TText::GoNextLink()  // ������� � ��������� ������ �� Next
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetNext();
    }
    return RetCode;
}

int TText::GoPrevLink()  // ������� � ���������� ������� � ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent == pFirst)
        SetRetCode(DataErr);
    else
    {
        pCurrent = Path.top();
        Path.pop();
    }
    return RetCode;
}

// ������
string TText::GetLine()   // ������ ������� ������
{
    if (pCurrent == nullptr)
    {
        SetRetCode(DataErr);
        return "";
    }
    else
        return string(pCurrent->Str);
}

void TText::SetLine(string s) // ������ ������� ������ 
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
        strcpy_s(pCurrent->Str, s.c_str());
}

// �����������
void TText::InsDownLine(string s)    // ������� ������ � ����������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
    }
}

void TText::InsDownSection(string s) // ������� ������� � ����������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
    }
}

void TText::InsNextLine(string s)    // ������� ������ � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
    }
}

void TText::InsNextSection(string s) // ������� ������� � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
    }
}

void TText::DelDownLine()        // �������� ������ � ���������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown->IsAtom())
        pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection()     // �������� ������� � ���������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pDown = nullptr;
}

void TText::DelNextLine()        // �������� ������ � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection()     // �������� ������� � ��� �� ������
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

// ��������
int TText::Reset()              // ���������� �� ������ �������
{
    GoFirstLink();
    if (pCurrent != nullptr)
    {
        St.push(pCurrent);
        if (pCurrent->pNext != nullptr)
            St.push(pCurrent->pNext);
        if (pCurrent->pDown != nullptr)
            St.push(pCurrent->pDown);
    }
    return RetCode;
}

bool TText::IsTextEnded() const  // ����� ��������?
{
    return St.empty();
}

int TText::GoNext()             // ������� � ��������� ������
{
    if (!IsTextEnded())
    {
        pCurrent = St.top();
        St.pop();
        if (pCurrent != pFirst)
        {
            if (pCurrent->pNext != nullptr)
                St.push(pCurrent->pNext);
            if (pCurrent->pDown != nullptr)
                St.push(pCurrent->pDown);
        }

    }
    return IsTextEnded();
}

//������ � �������
void TText::Read(char * pFileName)  // ���� ������ �� �����
{
    ifstream TextFile(pFileName);
    TextLevel = 0;
    if (TextFile)
        pFirst = ReadText(TextFile);
}

void TText::Write(char * pFileName) // ����� ������ � ����
{
    ofstream TextFile(pFileName);
    TextLevel = 0;
    if (TextFile)
        PrintTextInFile(pFirst, TextFile);
}

//������
void TText::Print()             // ������ ������
{
    TextLevel = 0;
    PrintText(pFirst);
}
